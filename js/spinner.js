var spinner = (function (doc, wndow) {

    return {
        activate: function () {

            $(".spinner").addClass("active");
        },
        deactivate: function () {
            $(".spinner").removeClass("active");
        }
    };
})(document, window);